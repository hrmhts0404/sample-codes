package main

import (
	"time"
	"testing"
)

func TestSetItem(t *testing.T) {
	memdb := InitMemdb(600)
	m     := &memdb
	info  := UserInfo{"Boy","mypass"}

	set   := m.Set(&info)
	if !set{
		t.Errorf("Failed to Set")
	}
	found := m.Get(&info)
	if !found{
		t.Errorf("Failed to find.")
	}
}

func TestWrongPass(t *testing.T) {
	memdb := InitMemdb(600)
	m     := &memdb
	info  := UserInfo{"Kid","pass"}

	m.Set(&info)
	info.Pass = "wrong"

	found := m.Get(&info)
	if found{
		t.Errorf("Failed to find.")
	}

}

func TestSetSome(t *testing.T) {
	arr  := [5]string{"Galois","Euler","Gauss","Rieman","Gurtandiek",}
	mdb  := InitMemdb(600)
	m    := &mdb

	for _,v := range arr  {
		info := UserInfo{v,v}
		m.Set(&info)
	}

	if len(m.data) != 5 {
		t.Errorf("Failed to Set some key. Len(Table) is less.")
	}

	for _,v := range arr {
		info := UserInfo{v,v}
		found := m.Get(&info)
		if !found {
			t.Errorf("Failed to Set. Key not found")
		}
	}

}

func TestFailWithSameName(t *testing.T) {
	mdb := InitMemdb(600)
	m   := &mdb
	ui  := UserInfo{"sameName", "somepass"}

	m.Set(&ui)
	failed := !(m.Set(&ui))

	if !failed {
		t.Errorf("Failed to fail with double Set")
	}

}

func TestExpiration(t *testing.T) {
	mdb := InitMemdb(0)
	m   := &mdb
	info := UserInfo{"name","pass"}

	m.Set(&info)
	time.Sleep(1 * time.Second)

	found := m.Get(&info)
	if found {
		t.Errorf("Failed to expire the key.")
	}
}
