package main

import (
	"fmt"
	jwt "github.com/golang-jwt/jwt"
)

type ResourceDB struct{
	data string
}
func (db *ResourceDB) Get() string{
	return db.data
}

type ResourceServer struct{
	CommonKey []byte
	method    jwt.SigningMethod
	db        ResourceDB
}

func InitResourceServer(key []byte, resource string) ResourceServer{
	return ResourceServer{
		CommonKey: key,
		method:    jwt.SigningMethodHS512,
		db:        ResourceDB{resource},
	}
}

func (rs *ResourceServer) GetData(tokenstr string) (ok bool, resource string) {
	var claims AccessTokenPayload
	token, err := jwt.ParseWithClaims(tokenstr, &claims, func(token *jwt.Token) (interface{}, error) {
		// Confirm Signing method for sure.
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		// rs.CommonKey
		return CommonAPIKey, nil
	})

	// Verify the token with claims field and signing.
	if err != nil || !token.Valid {
		return false, resource
	}
	// Verify the token with claims values.
	now := unow()
	if  (claims.ExpiresAt <= now)     ||
		(claims.NotBefore > now)      ||
		(claims.Issuer   != AuthName) ||
		(claims.Audience != ResourceName) {
		return false, resource
	}
	return true, rs.db.data
}

