package main

import (
	"crypto/sha256"
)

const s224 = sha256.Size224
// Get salt from env or stdin and hash it.
var dbsalt = []byte("任意の開被覆に対してその有限部分被覆を取ることが出来る=コンパクト")

type Entry struct{
	ttl  int64
	name string
	hash [s224]byte
}

type Memdb struct{
	data map[string]Entry
	ttl  int64
}

func InitMemdb(ttl int64) Memdb {
	return Memdb{
		data: make(map[string]Entry),
		ttl:  ttl,
	}
}

func (m *Memdb) Set(ui *UserInfo) bool {
	now := unow()

	// If not-expired key found, fail.
	entry, exist := m.data[ui.Name]
	if ( exist && entry.ttl >= now) {return false}

	entry = Entry{
		ttl:  now + m.ttl,
		name: ui.Name,
		hash: sha(ui.Pass),
	}
	m.data[ui.Name] = entry
	return true
}

func (m *Memdb) Get(ui *UserInfo) bool {
	entry, found := m.data[ui.Name]
	if !found {return false}
	if (entry.ttl < unow()) {
		delete(m.data, ui.Name)
		return false
	}
	return compareHash(entry.hash, sha(ui.Pass))
}

func sha(pass string) [s224]byte {
	secret := []byte(pass)

	// TODO more efficient way?
	for _, b := range dbsalt {
		secret = append(secret, b)
	}

	return sha256.Sum224(secret)
}

func compareHash(x, y [s224]byte) bool {
	i := 0
	for (i < s224) {
		if (x[i] != y[i]) {return false}
		i += 1
	}
	return true
}
