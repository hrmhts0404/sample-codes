package main

import (
	"fmt"
	"time"
	jwt "github.com/golang-jwt/jwt"
	"testing"
)

func tMintToken(key []byte, name string, ttl int64) (string, error) {
	orig := "hello " + name
	now := unow()
	claims := AccessTokenPayload {
		orig,
		jwt.StandardClaims{
			Issuer:    AuthName,
			Subject:   name,
			Audience:  ResourceName,
			IssuedAt:  now,
			ExpiresAt: now + ttl,
			NotBefore: now,
			Id:        fmt.Sprint(unow()) + ":" + name,
		},
	}
	tokenObj := jwt.NewWithClaims(jwt.SigningMethodHS512, claims)
	return tokenObj.SignedString(key)
}

func TestGetWithValidToken(t *testing.T) {
	rsrc := InitResourceServer(CommonAPIKey, "Secret Info")
	rs   := &rsrc

	token, _ := tMintToken(CommonAPIKey, "User", 10)

	ok, resource := rs.GetData(token)
	if !ok {
		t.Errorf("Failed to verify token.")
	}
	if (resource != "Secret Info") {
		t.Errorf("Failed to return secret info.")
	}
}

func TestGetWithInvalidToken(t *testing.T) {
	rsrc := InitResourceServer(CommonAPIKey, "Some Info")
	rs   := &rsrc

	token, _ := tMintToken([]byte("WrongKey"), "User", 10)

	passed, _ := rs.GetData(token)
	if passed {
		t.Errorf("Verify with WrongKey Passed.")
	}

	token, _ = tMintToken(CommonAPIKey, "User", 0)
	time.Sleep(50 * time.Millisecond)
	passed, _ = rs.GetData(token)
	if passed {
		t.Errorf("Verify with expired passed.")
	}
}
