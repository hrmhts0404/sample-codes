# Jwt Server Demo

## Explain about the commits.

### 1. Let a mapping pretend DB with expiration.

To make a logic simple and consice, we do not use a dabase with some sockets and on some process... stuff.

Let's wrap a mapping type to use it as a DB with expiration. 

API can be like this.

``` go
ui := UserInfo{"name","pass"}

memdb := InitMemdb(600) // 600 sec time to live.
m     := &memdb

ok    := m.Set(ui) // Hash pass here.
found := m.Get(ui) // Does NOT return entry. just bool.
```

Add `memdb_test.go` first, and implment `memdb.go`.

### 2. Make AuthServer to mint AuthToken.

Let's make an server to mint auth-token using "golang-jwt/jwt".

API can be like this. (For later, the api will be modified.)

``` go
// 600 for db ttl, 15 for token expiration.
auth := InitAuthServer([]byte("CommonAPIKey"), 600, 15)
a    := &auth

ok, failmsg := a.RegisterUser(&userinfo)
ok, token   := a.Login(&userinfo)
```

### 3. Make ResourceServer which verifies the token.

Then make a server to return some secret info after verifying the auth-token.

API can be like this.

``` go
rsrc := InitResourceServer([]byte("CommonAPIKey"), "Secret-Info")
rs   := &rsrc

ok, resource := rs.GetData(usertoken) // usertoken: string
```

### 4. Setup a http server that Jwt-demo runs on.

Before start to implement refresh token facility onto `authserver`, setup http server with jwt-demo actually runs on.

Create `main.go` file and put a http server. Main code looks like below.

``` go
func main() {

    demo := InitJwtDemo( ...someInitArgs )
    d    := &demo
    
    http.HandleFunc("/", indexHandler)
    // APIs
    http.HandleFunc("/auth/register", d.cbRegister)
    http.HandleFunc("/auth/login",    d.cbLogin)
    http.HandleFunc("/rsrc/get"  ,    d.cbGetResource)
    
    err := http.ListenAndServe(":8080", nil)
    if err != nil {...}

}
```

### 5. Add Refresh-Token facility on server-side.

To follow RFC6749, let's add refresh-token facility on a server. (But in this demo, we will not implement token-revocation. Thus this demo does NOT follow RFC6749 strictly.)

Our TODO are,

 - Add `mintRefreshToken` in `authserver.go`
 - Make `Login` return both `auth` and `refresh` tokens in `authserver`
 - Add and modify `authserver_test.go`.
 - Modify a few types in `common.go`, and some methods in `main.go`
 
We don't touch front-UI for now.

### 6. Finish up with modifying front-UI.


