package main

import (
	"fmt"
	jwt "github.com/golang-jwt/jwt"
)

// Read from ENV or stdin and hash it.
var refreshKey = []byte("Dim(V) = Rank(V) + Ker(V)")

type AuthServer struct{
	userDB Memdb
	method jwt.SigningMethod
	CommonKey   []byte
	DomesticKey []byte
	authttl int64
	refttl  int64
}

func InitAuthServer(common []byte,
	dbttl int64, refttl int64,
	authttl int64) AuthServer {
	return AuthServer{
		userDB: InitMemdb(dbttl),
		method: jwt.SigningMethodHS512,
		CommonKey:  common,
		DomesticKey:refreshKey,
		authttl: authttl,
		refttl:  refttl,
	}
}

func (a *AuthServer) RegisterUser(ui *UserInfo) (ok bool, failmsg string) {
	// Check Name
	if !a.userDB.Set(ui) {
		return false, "The name is already used."
	}
	return true, failmsg// emtpy
}

func (a *AuthServer) Login(ui *UserInfo) (ok bool, authtoken string, reftoken string) {
	// Check the account.
	if !( a.userDB.Get(ui) ) {return false, authtoken, reftoken}

	reftoken, err := a.mintRefreshToken(ui.Name)
	if err != nil {
		return false, "",""
	}
	authtoken, err = a.mintAuthToken(ui.Name)
	if err != nil {
		return false, "", ""
	}

	return true, authtoken, reftoken
}

func (a *AuthServer) Refresh(tokenstr string) (ok bool, authtoken string) {
	// Verify a refresh-token.
	var claims AccessTokenPayload
	refToken, err := jwt.ParseWithClaims(tokenstr, &claims, func(refToken *jwt.Token) (interface{}, error){
		// Confirm Singning method for sure.
		if _, ok := refToken.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpectedd signing method: %v", refToken.Header["alg"])
		}
		return a.DomesticKey, nil
	})
	if err != nil || !refToken.Valid {
		return false, authtoken
	}

	name := claims.Original
	now  := unow()
	if  (claims.NotBefore <= now)  ||
		(claims.ExpiresAt >  now)  ||
		(claims.Subject  == name)     ||
		(claims.Issuer   == AuthName) ||
		(claims.Audience == AuthName) {
		authtoken, err = a.mintAuthToken(name)
		if err == nil {
			return true, authtoken
		}
		return false, ""
	}
	return false, authtoken
}

func (a *AuthServer) mintAuthToken(name string) (token string, err error) {
	orig := "hello " + name
	now := unow()
	claims := AccessTokenPayload{
		orig,
		jwt.StandardClaims{
			Issuer:    AuthName,
			Subject:   name,
			Audience:  ResourceName,
			IssuedAt:  now,
			ExpiresAt: now + a.authttl,
			NotBefore: now,
			Id:        fmt.Sprint(now) + ":" + name,
		},
	}
	tokenObj := jwt.NewWithClaims(a.method, claims)
	return tokenObj.SignedString(a.CommonKey)
}

func (a *AuthServer) mintRefreshToken(name string) (token string, err error) {
	orig := name
	now  := unow()
	claims := AccessTokenPayload {
		orig,
		jwt.StandardClaims{
			Issuer:    AuthName,
			Subject:   name,
			Audience:  AuthName,
			IssuedAt:  now,
			ExpiresAt: now + a.refttl,
			NotBefore: now,
			Id:        fmt.Sprint(now) + ":" + name,
		},
	}
	tokenObj := jwt.NewWithClaims(a.method, claims)
	return tokenObj.SignedString(a.DomesticKey)
}
