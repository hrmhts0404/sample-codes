package main

import (
	"time"
	jwt "github.com/golang-jwt/jwt"
)
const (
	AuthName     = "JwtDemo-Authority"
	ResourceName = "JwtDemo-Resource"
)
var CommonAPIKey = []byte("圏Cの任意のFanctor F:C->Set に対して、自然な写像 (ゴニョゴニョ...) は全単射となる。")

type ClientRequest struct{
	Token string `json:"token"`
}
type ServerResponse struct{
	Ok   bool   `json:"ok"`
	Msg  string `json:"msg ,omitempty"`
	Data string `json:"data,omitempty"`
	Auth string `json:"auth,omitempty"`
	Refr string `json:"ref,omitempty"`
}

type UserInfo struct{
	Name string `json:"name"`
	Pass string `json:"pass"`
}
type AccessTokenPayload struct{
	Original string `json:"orig"`
	jwt.StandardClaims
	//struct {
	//	Audience  string `json:"aud,omitempty"`
	//	ExpiresAt int64  `json:"exp,omitempty"`
	//	Id        string `json:"jti,omitempty"`
	//	IssuedAt  int64  `json:"iat,omitempty"`
	//	Issuer    string `json:"iss,omitempty"`
	//	NotBefore int64  `json:"nbf,omitempty"`
	//	Subject   string `json:"sub,omitempty"`
}
func unow() int64 {
	return time.Now().Unix()
}
