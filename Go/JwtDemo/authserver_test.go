package main

import (
	"fmt"
	jwt "github.com/golang-jwt/jwt"
	"testing"
)

func TestRegisterAndLogin(t *testing.T) {
	auth := InitAuthServer(CommonAPIKey, 600, 30, 10)
	a    := &auth
	ui   := UserInfo{"Name", "Pass"}

	ok, msg := a.RegisterUser(&ui)
	if !ok {
		t.Errorf("Somewhat failed to register : " + msg)
	}

	ok, authtoken, reftoken := a.Login(&ui)
	if !ok {
		t.Errorf("Somewaht filed to login : " + msg)
	}

	ok, authtoken = a.Refresh(reftoken)
	if !ok {
		t.Errorf("Failed to refresh")
		return
	}

	var claims AccessTokenPayload
	token, err := jwt.ParseWithClaims(authtoken, &claims, func(token *jwt.Token) (interface{}, error) {
		// Confirm Signing method for sure.
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return CommonAPIKey, nil
	})

	// Verify the token with claims field and signing.
	if err != nil {
		t.Errorf("Failed to Parse Claims %e", err)
	}
	if !token.Valid {
		t.Errorf("Token is judged as invalid.")
	}
	// Verify the token with claims values.
	now := unow()
	if (claims.ExpiresAt <= now) || (claims.NotBefore > now) {
		t.Errorf("Invalid token at time")
	}
	if (claims.Issuer != "JwtDemo-Authority") || (claims.Audience != "JwtDemo-Resource") {
		t.Errorf("Invalid token with iss, aud")
	}
	if (claims.Subject != "Name") {
		t.Errorf("Wrong name in claims %s", claims.Subject)
	}
}
