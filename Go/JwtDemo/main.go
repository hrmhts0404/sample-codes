package main

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
)

type JwtDemo struct {
	Auth AuthServer
	Rsrc ResourceServer
}

func InitJwtDemo(commonKey []byte,
	dbTTL int64,
	refTTL  int64,
	authTTL int64,
	resource string) JwtDemo {
	return JwtDemo{
		Auth: InitAuthServer(commonKey, dbTTL, refTTL, authTTL),
		Rsrc: InitResourceServer(commonKey, resource),
	}
}

func main() {

	// Account for 10min, Ref 30sec, Auth 10sec
	demo := InitJwtDemo(CommonAPIKey, 600, 30, 10, "Almost Top Secret")
	d := &demo

	http.HandleFunc("/", indexHandler)
	// APIs
	http.HandleFunc("/auth/register", d.cbRegister)
	http.HandleFunc("/auth/login", d.cbLogin)
	http.HandleFunc("/auth/refresh", d.cbRefresh)
	http.HandleFunc("/rsrc/get", d.cbGetResource)

	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		log.Fatal("lis n serv", err)
	}
}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "./index.html")
}

func (d *JwtDemo) cbRegister(w http.ResponseWriter, r *http.Request) {

	if r.Method != http.MethodPost {
		resp4XX(w, 404)
		return
	}
	buf, err := io.ReadAll(r.Body)
	if err != nil {
		resp4XX(w, 400)
		return
	}

	var request UserInfo
	var response ServerResponse

	json.Unmarshal(buf, &request)
	response.Ok, response.Msg = d.Auth.RegisterUser(&request)
	if !response.Ok {
		resp(w, &response, 401)
		return
	}

	_, response.Auth, response.Refr = d.Auth.Login(&request)
	resp(w, &response, 200)
	return
}

func (d *JwtDemo) cbLogin(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		resp4XX(w, 404)
		return
	}

	buf, err := io.ReadAll(r.Body)
	if err != nil {
		resp4XX(w, 400)
		return
	}

	var request UserInfo
	var response ServerResponse
	json.Unmarshal(buf, &request)
	response.Ok, response.Auth, response.Refr = d.Auth.Login(&request)
	if !response.Ok {
		resp4XX(w, 401)
		return
	}
	resp(w, &response, 200)
	return
}

func (d *JwtDemo) cbRefresh(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		resp4XX(w, 404)
		return
	}

	buf, err := io.ReadAll(r.Body)
	if err != nil {
		resp4XX(w, 400)
		return
	}

	var request  ClientRequest
	var response ServerResponse
	if err := json.Unmarshal(buf, &request); err != nil {
		resp4XX(w, 401)
	}
	response.Ok, response.Auth = d.Auth.Refresh(request.Token)
	if !response.Ok {
		resp4XX(w, 401)
		return
	}

	resp(w, &response, 200)
	return
}

func (d *JwtDemo) cbGetResource(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		resp4XX(w, 404)
		return
	}
	buf, err := io.ReadAll(r.Body)
	if err != nil {
		resp4XX(w, 400)
		return
	}

	var request ClientRequest
	var response ServerResponse

	json.Unmarshal(buf, &request)
	response.Ok, response.Data = d.Rsrc.GetData(request.Token)
	if !response.Ok {
		resp4XX(w, 401)
		return
	}

	resp(w, &response, 200)
	return
}

// To prevent auto-detection "Content-Type",
// let `ResponseWriter` know the content-type before
// `WriteHeader`ing.
func resp4XX(w http.ResponseWriter, status int) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	w.Write([]byte("{}"))
	return
}
func resp(w http.ResponseWriter, response *ServerResponse, status int) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	jsondata, _ := json.Marshal(*response)
	w.Write([]byte(string(jsondata)))
	return
}
